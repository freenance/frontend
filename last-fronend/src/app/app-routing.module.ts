import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlertsComponent } from './alerts/alerts.component';
import { HomePageComponent } from './home-page/home-page.component';
import { MissionsComponent } from './missions/missions.component';
import { OutputGraphComponent } from './outputgraph/outputgraph.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { UploadExcelComponent } from './upload-excel/upload-excel.component';

const routes: Routes = [{
  path: '', component: SignUpComponent
},
{
  path: 'login', component: SignInComponent
},
{
  path: 'home', component: HomePageComponent,
  children: [
    { path: 'dashboard', component: OutputGraphComponent },
    { path: 'missions', component: MissionsComponent },
    { path: 'alerts', component: AlertsComponent}
  ]
},

{ path: 'excel', component: UploadExcelComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
