import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApierService } from '../apier.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private router: Router, public apier: ApierService) { }


  ngOnInit(): void {
  }

  toExcel(){
    this.router.navigate(['excel'])
  }


  userEmail : string = '';
  userPassword : string = '';
  userName : string = '';
  public trySignUp() {
    var body = {'email': this.userEmail, 'password': this.userPassword, 'user_name': this.userName}
    this.apier.postSignUp(body);
    this.router.navigate(["home"]);
  }
}
