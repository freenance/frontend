import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputgraphComponent } from './outputgraph.component';

describe('OutputgraphComponent', () => {
  let component: OutputgraphComponent;
  let fixture: ComponentFixture<OutputgraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutputgraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputgraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
