import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { DataServiceService } from '../data-service.service';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-output-graph',
  templateUrl: './outputgraph.component.html',
  styleUrls: ['./outputgraph.component.css']
})
export class OutputGraphComponent implements OnInit {
  public options: any = {
    chart: {
      type: 'pie',
      height: 700
    },
    colors:["red","blue","green"],
    title: {
      text: 'פילוח הוצאות'
    },
    credits: {
      enabled: false
    },
    series: [{
      data: [{
        name: 'Chrome',
        y: 61.41,
        sliced: true,
        selected: true
      }, {
        name: 'Internet Explorer',
        y: 11.84
      }, {
        name: 'Firefox',
        y: 10.85
      }, {
        name: 'Edge',
        y: 4.67
      }, {
        name: 'Safari',
        y: 4.18
      }, {
        name: 'Sogou Explorer',
        y: 1.64
      }, {
        name: 'Opera',
        y: 1.6
      }, {
        name: 'QQ',
        y: 1.2
      }, {
        name: 'Other',
        y: 2.61
      }]
    }
    ]
  }
  constructor(public dataService: DataServiceService) { }

  ngOnInit() {
    let acceptedTasksSum: number = 0;
    let anomalSum: number = 0;
    let missionSum: number = 0;
    this.dataService.acceptedTasks.forEach(element => acceptedTasksSum += element.amount);
    this.dataService.anomalies.forEach(element => anomalSum += element.amount);
    this.dataService.missions.forEach(element => missionSum += element.amount);
    let series = [{
      data: [{
        name: 'התראות',
        y: anomalSum
      }, {
        name: 'משימות',
        y: missionSum
      }, {
        name: 'מאושר',
        y: acceptedTasksSum
      }]
    }
    ]
    this.options.series = series;
    Highcharts.chart('container', this.options);
  }
}