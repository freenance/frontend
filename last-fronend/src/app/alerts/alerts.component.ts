import { Component, OnInit } from '@angular/core';
import { Mission } from '../apier.service';
import { DataServiceService } from '../data-service.service';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {

  constructor(public dataservice:DataServiceService) { }

  missions: Array<Mission>
  keys: Array<string>
  ngOnInit(): void {
    this.missions = this.dataservice.missions;
    this.keys = Object.keys(this.dataservice.missions[0]);
  }

}
