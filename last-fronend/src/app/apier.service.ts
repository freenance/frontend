import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Data, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filed } from './upload-excel/upload-excel.component';

@Injectable({
  providedIn: 'root'
})
export class ApierService {

  constructor(public client: HttpClient, public router: Router) { }

  postSignIn(body: any): Observable<any> {
    return this.client.post("http://127.0.0.1:5000/check-user", JSON.stringify(body), { headers: { "Content-Type": "application/json" } });
  }


  enrich(name: string): Promise<any> {
    var json = JSON.stringify({ name: name });
    return this.client.post("http://127.0.0.1:5000/enrich", json, { headers: { "Content-Type": "application/json" } }).toPromise();
  }


  postSignUp(body: any) {
    this.client.post<AfterSignUp>("http://127.0.0.1:5000/create-user", JSON.stringify(body), { headers: { "Content-Type": "application/json" } }).subscribe(answer => {
      localStorage.setItem("id", answer.id.toString());
      this.router.navigate["excel"];
    })
  }

  postExcel(body: Array<filed>): Observable<DataFromServer> {
    var dataToSend = { data: body }
    return this.client.post<DataFromServer>("http://127.0.0.1:5000/upload-data", JSON.stringify(dataToSend), { headers: { "Content-Type": "application/json" } });
  }
}

export interface Mission {
  email: string,
  name: string,
  phone: string,
  website: string,
  occurances: Array<Date>,
  amount: number
}

export interface AfterSignUp {
  success: boolean,
  id: number
}
export interface DataFromServer {
  missions: Array<Mission>,
  anomalies: Array<filed>,
  normal_expenses: Array<any>,
  success: boolean
}
