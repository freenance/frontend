import { Injectable } from '@angular/core';
import { Mission } from './apier.service';
import { filed } from './upload-excel/upload-excel.component';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  constructor() { }

  public missions : Array<Mission> = [];
  public anomalies : Array<filed> = [];
  public acceptedTasks: Array<filed> =[];
}
