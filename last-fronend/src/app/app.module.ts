import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { HomePageComponent } from './home-page/home-page.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { MissionsComponent } from './missions/missions.component';
import { OutputGraphComponent } from './outputgraph/outputgraph.component';
import { HttpClientModule } from '@angular/common/http';
import { UploadExcelComponent } from './upload-excel/upload-excel.component';
import { AlertsComponent } from './alerts/alerts.component'

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    HomePageComponent,
    DashboardComponent,
    MainNavComponent,
    SignUpComponent,
    MissionsComponent,
    OutputGraphComponent,
    UploadExcelComponent,
    AlertsComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
