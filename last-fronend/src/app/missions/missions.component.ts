import { Component, OnInit } from '@angular/core';
import { ApierService, DataFromServer, Mission } from '../apier.service';
import { DataServiceService } from '../data-service.service';
import { filed } from '../upload-excel/upload-excel.component';

@Component({
  selector: 'app-missions',
  templateUrl: './missions.component.html',
  styleUrls: ['./missions.component.css']
})
export class MissionsComponent implements OnInit {

  constructor(public dataService: DataServiceService, public client: ApierService) { }

  public data: DataFromServer
  public keys: Array<string>
  ngOnInit(): void {
    this.data = history.state
    console.log(this.data[0]);
    this.keys = Object.keys(this.data.anomalies[0]);
    console.log(this.keys)
  }

  remove(row: filed) {
    this.dataService.acceptedTasks.push(row);
    this.data.anomalies = this.data.anomalies.filter(ele => ele.name != row.name);
    this.data.anomalies = [...this.data.anomalies];
  }

  async toMission(row: filed) {
    var mission: Mission = {name:'',email:'',occurances:[],phone:'',website:'',amount:0};
    await this.client.enrich(row.name).then(obs => {

      mission.name = row.name;
      mission.phone = obs.phone ? obs.phone : 'no data';
      mission.email = obs.email ? obs.email : 'no data';
      mission.website = obs.website ? obs.website : 'no data';
      mission.amount = row.amount;
      this.dataService.missions.push(mission);
      this.remove(row);
    }, error => {
      console.log(error);
      mission.name = row.name;
      mission.phone = 'no data';
      mission.email = 'no data';
      mission.website = 'no data';

      this.dataService.missions.push(mission);
      this.remove(row);
    })

  }
}

export interface Structure {
  "בית העסק": string;
  "תאריך העסקה": string;
  "סכום חיוב": number;
}
