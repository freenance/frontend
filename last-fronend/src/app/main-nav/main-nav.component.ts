import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataFromServer } from '../apier.service';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit {

  @Input() data: DataFromServer;
  constructor(public router: Router) { }

  ngOnInit(): void {
    console.log(this.data);
  }

  toMissions() {
    this.router.navigate(["home/missions"],{state:this.data});
  }

  toAlerts(){
    this.router.navigate(["home/alerts"],{state:this.data});
  }
}
