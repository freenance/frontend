import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Dictionary } from 'highcharts';
import * as XLSX from 'xlsx';
import { ApierService, DataFromServer } from '../apier.service';
import { DataServiceService } from '../data-service.service';

@Component({
  selector: 'app-upload-excel',
  templateUrl: './upload-excel.component.html',
  styleUrls: ['./upload-excel.component.css']
})
export class UploadExcelComponent implements OnInit {


  constructor(public router: Router, public apier: ApierService, public dataService: DataServiceService) { }

  ngOnInit(): void {
  }

  dict: Dictionary<string> = { "בית העסק": "name", "תאריך העסקה": "date", "סכום החיוב": "amount" }
  arrayBuffer: any
  file: any;
  filelist: any;
  async addFile(event: any) {
    this.file = event.target.files[0];
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer(this.file);
    fileReader.onload = async (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();

      for (var i = 0; i != data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var arraylist: Array<any> = XLSX.utils.sheet_to_json(worksheet, { raw: false,dateNF: 'dd/MM/YYYY' });
      console.log("arraylist:")
      console.log(arraylist)

      arraylist.forEach(element => {
        delete element["סכום העסקה"];
        delete element["תאריך חיוב"];
      })
      var keys = Object.keys(arraylist[0]);

      arraylist.forEach(element => {
        keys.forEach(key => {
          element[this.dict[key]] = element[key];
          delete element[key]
        })
        element["amount"] = parseFloat(element["amount"])
      });

      var dataFromServer: DataFromServer;
      await this.apier.postExcel(arraylist).toPromise().then(obs => {
        dataFromServer = obs;
        this.router.navigate(["home"], { state: dataFromServer });

        this.dataService.anomalies = [...obs.anomalies];
      }, error => {
        console.log(error);
      });
      console.log(arraylist);

    }
  }
}
export interface filed {
  amount: number;
  date: Date;
  name: string;
}


/**let numbersCounters: Array<filed> = [];
      let dateTimecounters: Array<filed> = [];
      var counters: Array<number> = new Array<number>(keys.length);
      for (let i = 0; i < counters.length; i++) {
        switch (typeof arraylist[0][keys[i]]) {
          case "number":
            numbersCounters.push({fieldName: keys[i],sum:0,type: "number"});
            break;
          case "string":
            var date = new Date(arraylist[0][keys[i]].split("/").reverse());
            if (!isNaN(date.getTime())) {
              dateTimecounters.push({fieldName: keys[i],sum:0,type: "Datetime"});
            }
            break;
        }

        types[i] = typeof arraylist[0][keys[i]];
        counters[i] = 0;
        dateTimecounters[i]
      }
      arraylist.forEach(function (row: any) {
        for (let i = 0; i < keys.length; i++) {
          dateTimecounters[0].fieldName

        }
      })

      var obj = keys.map(function (key, index) {
        return {
          [types[index]]: counters[index]
        }
      })

      var groupBy = function (xs: any, key: any) {
        return xs.reduce(function (rv: any, x: any) {
          (rv[x[key]] = rv[x[key]] || []).push(x);
          return rv;
        }, {});
      };

      var t = groupBy(obj, "Datetime");
 */