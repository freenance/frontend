import { TestBed } from '@angular/core/testing';

import { ApierService } from './apier.service';

describe('ApierService', () => {
  let service: ApierService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApierService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
