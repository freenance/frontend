import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApierService } from '../apier.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  
  constructor(public apier: ApierService,public router:Router) {

  }

  ngOnInit(): void {
  }

  errorMessage: string = '';
  userEmail : string = '';
  userPassword : string = '';
  public trySignIn() {
    var body = {'email': this.userEmail, 'password': this.userPassword}
    this.apier.postSignIn(body).subscribe(answer=>{
      console.log(answer)
      localStorage.setItem("id",answer.id.toString());
      this.router.navigate(["excel"])
    },error =>{
      console.log(error)
      this.errorMessage = 'אימייל או סיסמא שגויים, אנא נסי שוב'
    });
    
  }
}
